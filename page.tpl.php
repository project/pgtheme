<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
	<!--[if IE 7]>
	  <link rel="stylesheet" type="text/css" href="<?php print base_path() . path_to_theme() ?>/iefixes.css" />
	<![endif]-->
	<!--[if IE 6]>
	  <link rel="stylesheet" type="text/css" href="<?php print base_path() . path_to_theme() ?>/ie6fixes.css" />
	<![endif]-->
	
  </head>
  <body<?php print phptemplate_body_class($left, $right); ?>>		      		
	<div id="pgContainerWrap">
	  <div id="pgContainer">
  		<div id="pgHeaderContainer">
    	  <div id="pgHeader">
			<div id="pgHeaderLogoLeft">
			  <a href="<?php print check_url($front_page) ?>" title="<?php print $site_slogan ?>"><?php print $site_name ?></a>
			</div>
			<div id="pgHeaderLogoRight"></div>
		  </div><!-- /header -->
		  <!-- pgTopNav -->
		  <div id="pgTopNav">
			<div id="pgTopNavLeft"></div>
			<div id="pgTopNavRight"></div>
	  		<?php if (isset($primary_links)) : ?>
	    	  <?php //print theme('linksnew', $primary_links, array('class' => 'links primary-links')) ?>
			  <?php print theme('links', $primary_links, array('id' => 'pgTopNavList')) ?>
	  		<?php endif; ?>
		  </div>
		  <!-- /pgTopNav-->
  		</div><!-- /wrapper-header -->

		<div id="pgMainWrap"> 
			<div id="pgMainContainer"> 
	  	<?php if ($header): ?>
				<div id="topbar">
	    <?php print $header; ?>
				</div>
	  	<?php endif; ?> 

			<?php if ($left): ?>
				<div id="sidebar-left" class="sidebar">
			<?php print $left ?>
      	</div><!-- /sidebar-left -->
      <?php endif; ?>

			<div id="center">		  
		  	<?php if ($breadcrumb): print $breadcrumb; endif; ?>
				<?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
				<?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
				<?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
			    <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
                <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
                <?php if ($show_messages && $messages): print $messages; endif; ?>
				<?php if ($help): print $help; endif; ?>
				<?php print $content ?>
				<span class="clear"></span>
				<?php print $feed_icons ?>
				<p><a href="#top" class="to-top"><?php print t('Back to top'); ?></a></p>
      		  </div><!-- /center -->

   	  <?php if ($right): ?>
     	  <div id="sidebar-right" class="sidebar">
   	  <?php print $right ?>
     	  </div><!-- /sidebar-right -->
   	  <?php endif; ?>

			<div class="clear"></div>
			</div><!-- /main -->

		  <div id="pgFooter"> 
      	  <?php print $footer_message . $footer  ?>
		  </div>
  		</div><!-- /wrapper-main -->
	  </div>
	</div>
  <?php print $closure ?>	
  </body>
</html>