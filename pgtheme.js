$(document).ready(function(){
	$("div[id^='block-']").corner("8px");

	$("div[id^='block-block-']").find(".content").addClass("block-list-color").find("ul:first").addClass("menu");
	$("div[id^='block-user-']").find(".content").addClass("block-list-color");

	$("div[id^='block-user-'] ul.menu:first > li:not(.expanded)").addClass("block-list-border-bottom");
	$("div[id^='block-user-'] ul.menu:first > li:last").removeClass("block-list-border-bottom");

	$("div[id^='block-block-'] ul.menu:first > li:not(.expanded)").addClass("block-list-border-bottom");
	$("div[id^='block-block-'] ul.menu:first > li:last").removeClass("block-list-border-bottom");
});


